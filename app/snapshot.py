import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import FirefoxOptions
import time
import os

def make_snapshot(service_id, metrics_tag, grafana_url, user, pwd, timeout=4, dashboard_uid="RnTZ3cbGk", range_from="now-2d", range_to="now", logger=None):
  
  logger.debug("Init Firefox")
  opts = FirefoxOptions()
  opts.headless = True
  opts.add_argument("window-size=1400,600")
  driver = webdriver.Firefox(firefox_options=opts)

  #Open the website
  logger.debug("Open the website")
  driver.get(grafana_url)
  driver.set_window_size(1920, 1080)
  #login
  logger.debug("Login")
  driver.find_element_by_name("user").send_keys(user)
  driver.find_element_by_name ("password").send_keys(pwd)
  driver.find_elements_by_tag_name("button")[0].click()
  
  #select dashbaord
  logger.debug("Select dashbaord")
  dashboard_url = f'{grafana_url}d/{dashboard_uid}/service-statistics?orgId=1&var-service_id={service_id}&var-metrics_tag={metrics_tag}&var-range=5m&from={range_from}&to={range_to}'
  logger.debug(f"Dasboard url: {dashboard_url}")
  driver.get(dashboard_url)
  
  #wait until new page has loaded
  logger.debug("Wait until new page has loaded")
  button = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[3]/div[1]/div/div[3]/div/button')))

  #select share
  logger.debug("Select share")
  button.click()

  #click on snapshots
  logger.debug("Click on snapshots")
  button = driver.find_element_by_xpath("/html/body/div/div/div[1]/div[1]/div[1]/ul/li[2]")
  button.click()

  #select 1h timeout
  # logger.debug("Select 1h timeout")
  # button = driver.find_element_by_xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[3]/div[2]/div/div/div[1]/div[1]/div")
  # driver.execute_script("arguments[0].innerHTML = arguments[1]", button, "1 Hour")

  #click on local snapshot
  logger.debug("click on local snapshot")
  button = driver.find_element_by_xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[5]/div/div[3]/button")
  button.click()
  
  #wait for snapshot to be complete
  logger.debug("wait for snapshot to be complete")
  link = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div/div[1]/div[2]/div/div[1]/div/a')))

  #return snapshot URL
  logger.debug("return snapshot URL")
  url = link.get_attribute('href')
    
  #close the browser
  logger.debug(f"close the browser")
  driver.quit()
  
  logger.debug(f"Return {url}")
  return url


