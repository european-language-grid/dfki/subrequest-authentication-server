from typing import Optional
import requests
import re
import json
from loguru import logger
from elg import Entity
import hashlib
import os 

from fastapi import FastAPI, Header, HTTPException, Request
from fastapi.responses import RedirectResponse

from .snapshot import make_snapshot

app = FastAPI()

DOMAIN = os.getenv('DOMAIN')
TOKEN = os.getenv("GRAFANA_TOKEN")
PWD = os.getenv("GRAFANA_ADMIN_PWD")
DASHBOARD_UID = "-kY-DGz7z"


@app.get("/{service_id}")
async def auth(service_id: int, request: Request):
    """
    Authentication subrequest
    """    
    # Check Keycloak token
    logger.info("Check Keycloak token")
    logger.debug(f"Headers: {request.headers}")
    auth = request.headers.get("Authorization")
    if auth is None:
        auth = request.headers.get("authorization")
    if auth is None:
        raise HTTPException(status_code=400, detail=f"Authorization token not provided")
    headers = {"Authorization": auth}
    url = f"https://{DOMAIN}.european-language-grid.eu/auth/realms/ELG/protocol/openid-connect/userinfo"
    response = requests.get(url, headers=headers)
    logger.debug(f"Response: {response.content}")
    if not response.ok:
        if response.status_code == 400:
            raise HTTPException(status_code=400, detail=f"Token not provided - {response.json()}")
        elif response.status_code == 401:
            raise HTTPException(status_code=400, detail=f"Token not valid - {response.json()}")
        else:
            raise HTTPException(status_code=404, detail=f"{response.json()}")
    logger.info(f"KC: {response.json()}")
    admin = True if "admin" in response.json().get("roles") else False
    user_email = response.json().get("email")
    
    # Check service
    logger.info("Check service")    
    logger.debug(f"Service id: {service_id}")
    try:
        logger.debug(f"Get metadatarecord: {service_id}")
        entity = Entity.from_id(service_id, domain=DOMAIN, use_cache=False)
        logger.debug(f"extract docker image info")
        docker_image = entity.record["described_entity"]["lr_subclass"]["software_distribution"][0]["docker_download_location"]
        logger.debug("extract providers info")
        providers = []
        for p in entity.record["metadata_curator"]:
            logger.debug(f"provid: {p}")
            providers += p["email"]
    except:
        raise HTTPException(status_code=500, detail=f"The service [{service_id}] metadata record is not correct.")  
    logger.debug(f"Docker image: {docker_image}")
    logger.debug(f"providers: {providers}")
    
    if not admin and user_email not in providers:
        raise HTTPException(status_code=401, detail=f"You can not access the dashboard of the service {service_id}.")  
    
    
    # Create the snapshot
    logger.info("Create the snapshot")
    try:
        logger.debug("Create snapshot")
        snapshot_url = make_snapshot(
            service_id=service_id,
            metrics_tag=hashlib.sha1(str.encode(docker_image)).hexdigest(),
            grafana_url=f"https://{DOMAIN}.european-language-grid.eu/monitoring/",
            user="admin",
            pwd=PWD,
            timeout=16,
            dashboard_uid=DASHBOARD_UID, 
            range_from="now-2d", 
            range_to="now",
            logger=logger,
        )
    except Exception as e:
        logger.error(f"Error during the creation of the snapshot {e}")
        raise HTTPException(status_code=500, detail="Error when creating the snapshot")
    logger.debug(f"Snapshot url: {snapshot_url}")
    return RedirectResponse(snapshot_url)